import React, {Component} from 'react';
import Feed from './Feed';
import {connect} from 'react-redux';

class Content extends Component{
	render(){

		//Get active feed URL's from either state or localStorage, localStorage will help us to keep data when page gets refreshed
		let activeURL = this.props.activeURL ? this.props.activeURL : localStorage.getItem('activeURL');
		// console.log(activeURL);

		return (
			<div className="content">
				{this.props.loader &&
					<div className="loader"></div>
				}
				{activeURL && 
					<h4 className="text-center">{activeURL}</h4>
				}
				<Feed />
			</div>
		);
	}
}

const mapStateToProps = (state) =>{
	return {
		loader: state.loader,
		activeURL: state.activeURL,
	};
}

export default connect(mapStateToProps)(Content);