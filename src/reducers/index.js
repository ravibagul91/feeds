//Initial State
const initialState = {
	error:'',
	activeURL:'',
	activeFeeds:[],
	inactiveURL:'',
	inactiveFeeds:[],
	loader:false,
}

const reducers = (state= initialState,action) => {
	let newState = {...state}; //Make a copy of state
	switch(action.type){
		case 'LOADER':
		//Will display loader
			return Object.assign({},newState,{loader:true});
		case 'GET_FEEDS':
			let activeURL = localStorage.getItem('activeURL');
			if(activeURL===null || activeURL==="" || activeURL===undefined){
				// console.log(activeURL);
			}else{
				// console.log(activeURL);
				localStorage.setItem('inactiveURL',activeURL);
			}
			//Get Feeds and store it to localStorage, which will keep our data on page after page refresh 
			localStorage.setItem(action.activeURL,JSON.stringify(action.feeds));
			localStorage.setItem('activeURL',action.activeURL);
			//We also store this feeds in out state and return newstate, so that out component's get re-render
			return Object.assign({},newState,{activeFeeds:action.feeds,error:'',activeURL:action.activeURL,loader:false});
		case 'ERROR':
			//If invalid URL is passed, it simply display the error message 
			return Object.assign({},newState,{error:action.value,loader:false});
		case 'REMOVE_FEEDS':
			if(action.side==='active'){
				//If we click on'X' button on active feed it will get removed
				localStorage.removeItem(action.api_url);
				localStorage.removeItem('activeURL');
				let inactiveURL = localStorage.getItem('inactiveURL');
				let inactiveFeeds = JSON.parse(localStorage.getItem(inactiveURL));

				if(inactiveURL){
					//If we have next Inactive feed in localStorage, it will get active status
					localStorage.setItem('activeURL',inactiveURL);
					localStorage.removeItem('inactiveURL');
					return Object.assign({},newState,{error:'',activeURL:inactiveURL,inactiveURL:'',activeFeeds:inactiveFeeds,inactiveFeeds:[],loader:false});
				}else{
					//If we don't have next Inactive feed in localStorage, it will simply remove active feed
					return Object.assign({},newState,{error:'',activeURL:'',inactiveURL:'',activeFeeds:[],inactiveFeeds:[],loader:false});
				}
			}else{
				//If we click on'X' button on inactive feed will get removed
				localStorage.removeItem(action.api_url);
				localStorage.removeItem('inactiveURL');
				return Object.assign({},newState,{error:'',inactiveURL:'',inactiveFeeds:[],loader:false});
			}
		case 'CHANGE_FEEDS':
			if(action.side!=='active'){
				//If we click on inactive feed, it will changed to active feed and Active feed will get changed to inactive feed
				let activeURL = localStorage.getItem('activeURL');
				let inactiveURL = localStorage.getItem('inactiveURL');
				let inactiveFeeds = JSON.parse(localStorage.getItem(inactiveURL));
				let activeFeeds = JSON.parse(localStorage.getItem(activeURL));
				localStorage.setItem('activeURL',inactiveURL);
				localStorage.setItem('inactiveURL',activeURL);

				return Object.assign({},newState,{error:'',activeURL:inactiveURL,inactiveURL:activeURL,activeFeeds:inactiveFeeds,inactiveFeeds:activeFeeds,loader:false});
			}
			return newState;
		default:
			return newState;
	}
}

export default reducers;