import React from 'react'
import { connect } from 'react-redux'
import { getFeeds } from '../actions/actions'

const SearchBox = ({ error,dispatch }) => {
  let input;
  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault()
          if (!input.value.trim()) {
            return
          }
          //Dispatch an action to get feeds 
          dispatch(getFeeds(input.value));
          input.value = ''
        }}
      >
        <input ref={feed => (input = feed)} placeholder="Enter URL..."/>
        <button type="submit"><i className="fa fa-search"></i></button>
      </form>
      {error &&
      	<p style={{color:'red',fontSize:'12px'}}>{error}</p>
      }
    </div>
  )
}

const mapStateToProps = (state) =>{
  return {error:state.error};
}

export default connect(mapStateToProps)(SearchBox);