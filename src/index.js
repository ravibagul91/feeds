import React from 'react';
import ReactDOM from 'react-dom';
import App from './container/App';
import * as serviceWorker from './serviceWorker';

import {Provider} from 'react-redux';
import {createStore,applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

require('./sass/app.scss');

//Creating store, using reducers to handle actions on store.
//applyMiddleware(thunk) will provide a middleware, every action will pass thr' it, 
//if it is function then executes it otherwise executes next action.
const store = createStore(reducers,applyMiddleware(thunk));

//Provider will let store available to our APP
ReactDOM.render(<Provider store={store}>
          <App />
	</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
