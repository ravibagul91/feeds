import React, {Component} from 'react';
import SearchBox from './SearchBox';
import {connect} from 'react-redux';
import {removeFeed, changeFeed} from '../actions/actions';

class Sidebar extends Component{
	removeFeed(side,url){
		this.props.dispatch(removeFeed(side,url));
	}

	changeFeed(side,url){
		this.props.dispatch(changeFeed(side,url));
	}

	render(){
		//Get active and inactive feed URL's from either state or localStorage, localStorage will help us to keep data when page gets refreshed
		let activeURL = this.props.activeURL ? this.props.activeURL : localStorage.getItem('activeURL');
		let inactiveURL = this.props.inactiveURL ? this.props.inactiveURL : localStorage.getItem('inactiveURL');
		return (
			<div className="sidebar">
				<SearchBox/>
				<hr/>
				{activeURL ? <div className="mb-2" style={{border:'1px solid',borderRadius:'4px',maxWidth:'230px',position:'relative',cursor:'pointer',backgroundColor:'yellow'}} title={activeURL}>
					<div className="px-2" style={{maxWidth:'200px',wordBreak: 'break-all'}}>{activeURL}
					</div>
					<i className="fa fa-times" style={{position:'absolute',right:'5px',top:'5px'}} onClick={(e) => this.removeFeed('active',activeURL)}></i>
				</div>
				:
				''
				}
				{inactiveURL ? <div className="mb-2" style={{border:'1px solid',borderRadius:'4px',maxWidth:'230px',position:'relative',cursor:'pointer'}} title={inactiveURL}>
					<div className="px-2" style={{maxWidth:'200px',wordBreak: 'break-all'}} onClick={(e) => this.changeFeed('inactive',activeURL)}>{inactiveURL}
					</div>
					<i className="fa fa-times" style={{position:'absolute',right:'5px',top:'5px'}} onClick={(e) => this.removeFeed('inactive',inactiveURL)}></i>
				</div>
				:
				""
				}
			</div>
		);
	}
}

const mapStateToProps = (state) =>{
	return {
		activeURL: state.activeURL,
		inactiveURL: state.inactiveURL
	};
}


export default connect(mapStateToProps)(Sidebar);