import React from 'react';
import {connect} from 'react-redux';

const Feed = ({activeFeeds,activeURL}) =>{
	//Get active feed URL's and its content from either state or localStorage, localStorage will help us to keep data when page gets refreshed
	let headerURL = activeURL ? activeURL : localStorage.getItem('activeURL');
	let feeds = activeFeeds.length > 0 ? activeFeeds : JSON.parse(localStorage.getItem(headerURL));

	return (
		<div>
		{
			feeds ?
				feeds.map((nextFeed,index) =>{
					return(
						<div className="card m-4" key={index}>
					  		<div className="card-body">
					    		<h5 className="card-title"><strong>{nextFeed.title} - {nextFeed.pubDate}</strong></h5>
					    		<p className="card-text">{nextFeed.description}</p>
					  		</div>
						</div>
					)
				})
				:
				<h1 className=""><i className="fa fa-arrow-left mr-4"></i>Search RSS</h1>
		}
		</div>
	)
} 

const mapStateToProps = (state) =>{
	return {
		activeFeeds: state.activeFeeds,
		activeURL: state.activeURL,

	};
}

export default connect(mapStateToProps)(Feed);