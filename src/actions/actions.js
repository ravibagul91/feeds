//To show loader while executing any action
function showLoader(){
    return{
        type:'LOADER',
        value: true
    }
}

//Get feeds
export const getFeeds = (api_url) => {
	if(api_url!=='' && api_url!==undefined){
        return dispatch => {
            dispatch(showLoader());
            return fetch(api_url)
                .then(response => response.json())
                .then(res => dispatch({
                    type: 'GET_FEEDS',
                    feeds: res.items,
                    activeURL: api_url
                }))
                .catch(error => dispatch({type:'ERROR',value:'Invalid URL'}));
        }
	}
}

//Remove feeds
export const removeFeed = (side,api_url) => {
    return dispatch => {
        dispatch(showLoader());
        setTimeout(function(){
            return dispatch({
                type:'REMOVE_FEEDS',
                side: side,
                api_url: api_url
            });
        },1000)
    }
}

//Change feeds
export const changeFeed = (side,api_url) =>{
    return dispatch => {
        dispatch(showLoader());
        setTimeout(function(){
            return dispatch({
                type:'CHANGE_FEEDS',
                side: side,
                api_url: api_url
            });
        },1000)
    }
}

